#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

#include <netinet/in.h>
#include <ctime>
#include <thread>
#include <chrono>

#define EOL "\r\n"
#define EOL_SIZE 2

typedef struct {
	char *ext;
	char *mediatype;
} extn;

//Media files
extn extensions[] ={
	{"gif", "image/gif" },
	{"txt", "text/plain" },
	{"jpg", "image/jpg" },
	{"jpeg","image/jpeg"},
	{"png", "image/png" },
	{"ico", "image/ico" },
	{"zip", "image/zip" },
	{"gz",  "image/gz"  },
	{"tar", "image/tar" },
	{"htm", "text/html" },
	{"html","text/html" },
	{"php", "text/html" },
	{"pdf","application/pdf"},
	{"zip","application/octet-stream"},
	{"rar","application/octet-stream"},
	{0,0} };

void error(const char *msg) {
	perror(msg);
	exit(1);
}

int get_file_size(int fd) {
	struct stat stat_struct;
	if (fstat(fd, &stat_struct) == -1)
		return (1);
	return (int) stat_struct.st_size;
}

void send_new(int fd, char *msg) {
	int len = strlen(msg);
	//printf("len:%i, msg: %s\n",len, msg);
	if (send(fd, msg, len, 0) == -1) {
		printf("Error in send\n");
	}
}


int recv_new(int fd, char *buffer) {
	char *p = buffer; 
	int eol_matched = 0;
	while (recv(fd, p, 1, 0) != 0){
		if (*p == EOL[eol_matched]){
			++eol_matched;
			if (eol_matched == EOL_SIZE){
				*(p + 1 - EOL_SIZE) = '\0';
				return (strlen(buffer)); 
			}
		} 
		else{
			eol_matched = 0;
		}
		p++; 
	}
	return (0);
}

char* webroot() {
 	// open the file "conf" for reading
	FILE *in = fopen("conf", "rt");
 	// read the first line from the file
	char buff[1000];
	fgets(buff, 1000, in);
 	// close the stream
	fclose(in);
	char* nl_ptr = strrchr(buff, '\n');
	if (nl_ptr != NULL)
		*nl_ptr = '\0';
	return strdup(buff);
}

void php_cgi(char* script_path, int fd) {
	send_new(fd, "HTTP/1.1 200 OK\n Server: Web Server in C\n Connection: close\n");
	dup2(fd, STDOUT_FILENO);
	char script[500];
	strcpy(script, "SCRIPT_FILENAME=");
	strcat(script, script_path);
	putenv("GATEWAY_INTERFACE=CGI/1.1");
	putenv(script);
	putenv("QUERY_STRING=");
	putenv("REQUEST_METHOD=GET");
	putenv("REDIRECT_STATUS=true");
	putenv("SERVER_PROTOCOL=HTTP/1.1");
	putenv("REMOTE_HOST=127.0.0.1");
	execl("/usr/bin/php-cgi", "php-cgi", NULL);
}


int connection(int fd) {
	char request[500], resource[500], *ptr;
	int fd1, length, status;
	if (recv_new(fd, request) == 0) {
		printf("Recieve Failed\n");
	}
	printf("Request: %s\n", request);
 	// Check for a valid browser request
	ptr = strstr(request, " HTTP/");
		if (ptr == NULL) {
		printf("NOT HTTP !\n");
	} 
	else {
		*ptr = 0;
		ptr = NULL;

		if (strncmp(request, "GET ", 4) == 0) {
			ptr = request + 4;
		}
		if (ptr == NULL) {
			printf("Unknown Request ! \n");
		} 
		else {
			if (ptr[strlen(ptr) - 1] == '/') {
				strcat(ptr, "index.html");
			}
			strcpy(resource, webroot());
			strcat(resource, ptr);
			char* s = strchr(ptr, '.');
			int i;
			for (i = 0; extensions[i].ext != NULL; i++) {
				if (strcmp(s + 1, extensions[i].ext) == 0) {
					fd1 = open(resource, O_RDONLY, 0);
					printf("Opening \"%s\"\n", resource);
					if (fd1 == -1) {
						printf("404 File not found Error\n");
						send_new(fd, "HTTP/1.1 404 Not Found\r\n");
						send_new(fd, "Server : Web Server in C\r\n\r\n");
						send_new(fd, "<html><head><title>404 Not Found</head></title>");
						send_new(fd, "<body><p>404 Not Found: The requested resource could not be found!</p></body></html>");
      				//Handling php requests
					} else 
					if (strcmp(extensions[i].ext, "php") == 0) {
						php_cgi(resource, fd);
						sleep(1);
						close(fd);
						exit(1);
					} 
					else {
						printf("200 OK, Content-Type: %s\n\n", extensions[i].mediatype);
						send_new(fd, "HTTP/1.1 200 OK\r\n");
						send_new(fd, "Server : Web Server in C\r\n\r\n");
						char readBuffer[128];

						do {
							status = recv(fd, readBuffer, sizeof readBuffer, MSG_DONTWAIT);
						} while (status > 0);

    					if (ptr == request + 4){ // if it is a GET request
    						if ((length = get_file_size(fd1)) == -1)
    							printf("Error in getting size !\n");
    						size_t total_bytes_sent = 0;
    						ssize_t bytes_sent;
    						while (total_bytes_sent < length) {
    							if ((bytes_sent = sendfile(fd, fd1, 0, length - total_bytes_sent)) <= 0) {
    								if (errno == EINTR || errno == EAGAIN) {
    									continue;
    								}
    								perror("sendfile");
    								return -1;
    							}
    							total_bytes_sent += bytes_sent;
    						}

    					}
    				}
    				break;
    			}
    			int size = sizeof(extensions) / sizeof(extensions[0]);
    			if (i == size - 2) {
    				printf("415 Unsupported Media Type\n");
    				send_new(fd, "HTTP/1.1 415 Unsupported Media Type\r\n");
    				send_new(fd, "Server : Web Server in C\r\n\r\n");
    				send_new(fd, "<html><head><title>415 Unsupported Media Type</head></title>");
    				send_new(fd, "<body><p>415 Unsupported Media Type!</p></body></html>");
    			}
    		}

    		close(fd);
    	}
    }
    shutdown(fd, SHUT_RDWR);
}

int main(int argc, char *argv[]) {
	int sockfd, newsockfd, portno, pid, optval, n;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;

	if (argc < 2) {
		fprintf(stderr, "ERROR, no port provided\n");
		exit(1);
	}
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR on binding");
	listen(sockfd, 5);
	clilen = sizeof(cli_addr);

 if(getsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, &clilen) < 0) {
      perror("getsockopt()");
      close(sockfd);
      exit(EXIT_FAILURE);
   }
   printf("SO_KEEPALIVE is %s\n", (optval ? "ON" : "OFF"));

   /* Set the option active */
   optval = 1;
   clilen = sizeof(optval);
   if(setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, clilen) < 0) {
      perror("setsockopt()");
      close(sockfd);
      exit(EXIT_FAILURE);
   }
   printf("SO_KEEPALIVE set on socket\n");

   /* Check the status again */
   if(getsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, &clilen) < 0) {
      perror("getsockopt()");
      close(sockfd);
      exit(EXIT_FAILURE);
   }
   printf("SO_KEEPALIVE is %s\n", (optval ? "ON" : "OFF"));

  	while (1) {
  		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  		if (newsockfd < 0)
  			error("ERROR on accept");
  		pid = fork();
  		//printf("%i\n", pid);
  		if (pid < 0)
  			error("ERROR on fork");
  		if (pid == 0) {
  			//close(sockfd);
  			connection(newsockfd);
  			//exit(0);
  		} 
  		//else
  			//close(newsockfd);
  		while(1){
  			std::this_thread::sleep_for(std::chrono::seconds(10));
  			//printf("Message...\n");
  			std::time_t result = std::time(0);
  			std::string the_time(std::asctime(std::localtime(&result)));
  			//printf("It's the time...\n");
  			n = write(newsockfd,the_time.c_str(),50);
  			//printf("...\n");
  		}
  	}
  	close(sockfd);
 	return 0;
}
