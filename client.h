#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <thread> 

class Client{
public:
  Client(std::string ip_address, int port_number){
    ip = ip_address;
    port = port_number;
  }

  void init(){
    struct sockaddr_in stSockAddr;
    SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (-1 == SocketFD){
      perror("cannot create socket");
      exit(EXIT_FAILURE);
    }

    memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

    stSockAddr.sin_family = AF_INET;
    stSockAddr.sin_port = htons(port);
    int Res = inet_pton(AF_INET, ip.c_str(), &stSockAddr.sin_addr);

    if (0 > Res){
      perror("error: first parameter is not a valid address family");
      close(SocketFD);
      exit(EXIT_FAILURE);
    }
    else if (0 == Res){
      perror("char string (second parameter does not contain valid ipaddress");
        close(SocketFD);
        exit(EXIT_FAILURE);
      }
    if (-1 == connect(SocketFD, (const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in))){
      perror("connect failed");
      close(SocketFD);
      exit(EXIT_FAILURE);
    }

    //n = write(SocketFD,"Can I connect?",15);
    //n = read (SocketFD,buffer,20);
    //pos = std::stoi( buffer );
    //printf("My Id is:%i\n", pos);
    printf("I'm connected...\n");
  }

  void writing(){
    while(1){
      std::cin.getline (buffer,256);
      n = write(SocketFD,buffer,sizeof(buffer)-1);
    }
  }

  void reading(){
    //printf("Now I'm reading...\n");
    while(1){
      //printf("Waiting for something\n");
      n = read (SocketFD,buffer,50);
      printf("%s", buffer);
    }
  }

  void exe(){
    init();
    std::thread read (&Client::reading, this);
    //std::thread write (&Client::writing, this);
    read.join();
    //write.join();  
  }

  ~Client(){;}
private:
  std::string ip;
  int SocketFD;
  char buffer[256];
  int n, port, pos;
};